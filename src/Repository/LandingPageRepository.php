<?php declare(strict_types=1);

namespace Thepixeldeveloper\LandingPageBundle\Repository;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Thepixeldeveloper\LandingPageBundle\Entity\LandingPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * Class LandingPageRepository
 *
 * @package Thepixeldeveloper\LandingPageBundle\Repository
 */
class LandingPageRepository extends ServiceEntityRepository
{
    /**
     * LandingPageRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LandingPage::class);
    }
}
