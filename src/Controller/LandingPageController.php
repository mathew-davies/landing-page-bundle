<?php declare(strict_types=1);

namespace Thepixeldeveloper\LandingPageBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Thepixeldeveloper\LandingPageBundle\Entity\LandingPage;
use Thepixeldeveloper\LandingPageBundle\Form\LandingPageType;

/**
 * Class LandingPageController
 *
 * @package Thepixeldeveloper\LandingPageBundle\Controller
 */
class LandingPageController extends Controller
{
    /**
     * @Route("/", name="thepixeldeveloper_landing_page_bundle_controller")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \LogicException
     */
    public function index(Request $request): Response
    {
        $form = $this
            ->createForm(LandingPageType::class, new LandingPage())
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var ObjectManager $doctrine
             */
            $doctrine = $this->get('doctrine')->getManager();
            $doctrine->persist($form->getData());
            $doctrine->flush();

            /**
             * @var TranslatorInterface $translator
             */
            $translator = $this->get('translator');

            $this->addFlash('success', $translator->trans("Success! We'll give you a shout when it's ready."));

            return $this->redirectToRoute('thepixeldeveloper_landing_page_bundle_controller');
        }

        return $this->render('@ThepixeldeveloperLandingPage/landingPage.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
