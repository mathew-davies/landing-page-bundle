<?php declare(strict_types=1);

namespace Thepixeldeveloper\LandingPageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Thepixeldeveloper\LandingPageBundle\Repository\LandingPageRepository")
 * @UniqueEntity(fields={"email"}, message="This email address is already used.")
 */
class LandingPage
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", name="email", length=100)
     *
     * @Assert\Email(checkMX=true, checkHost=true)
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}
