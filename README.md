# Thepixeldeveloper\LandingPageBundle

[![pipeline status](https://gitlab.com/mathew-davies/landing-page-bundle/badges/master/pipeline.svg)](https://gitlab.com/mathew-davies/landing-page-bundle/commits/master)

A symfony bundle that integrates allows you to quickly throw out a landing page.
It takes an email and nothing else. Will potentially add support for other fields.

* [Installation](#installation)
* [Usage](#usage)

## Installation

1. Require as a composer dependency:

    ``` bash
    composer require "thepixeldeveloper/landing-page-bundle"
    ```

2. Register the bundle:

    ``` php
    # app/AppKernel.php
    public function registerBundles()
    {
        $bundles = [
            new Thepixeldeveloper\LandingPageBundle\ThepixeldeveloperLandingPageBundle(),
        ];
    }
    ```

3. Import routing

    ``` yaml
    # app/config/routing.yml
    thepixeldeveloper_landing_page_bundle:
        resource: "@ThepixeldeveloperLandingPageBundle/Controller/"
        type: annotation
        prefix:   /
    ```

## Usage

1. Generate the database schema using the doctrine schema tool.

2. Then override the `landingPage.html.twig` template with your landing page.
